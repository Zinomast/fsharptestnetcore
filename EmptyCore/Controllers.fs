namespace EmptyCore.Controllers

open System
open Microsoft.AspNetCore.Mvc

[<Route("[controller]")>]
type TestController() =
    inherit Controller()

    [<HttpGet>]
    member this.Get() =
        ["Hello", "World"]