namespace EmptyCore

open System.Threading.Tasks
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Logging
open Microsoft.Extensions.Configuration


type Startup(env: IHostingEnvironment) =
    let builder = ConfigurationBuilder()
                        .SetBasePath(env.ContentRootPath)
                        .AddJsonFile("appsettings.json", true, true)
                        .AddJsonFile((sprintf "appsettings.%s.json" env.EnvironmentName), true)
                        .AddEnvironmentVariables()

    let configuration = builder.Build()


    /// This method gets called by the runtime. Use this method to add services to the container.
    /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    member this.ConfigureServices(services:IServiceCollection) = 
            services.AddMvc()
            |> ignore
    

    /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    member this.Configure (app:IApplicationBuilder, env:IHostingEnvironment, loggerFactory: ILoggerFactory ) =
        loggerFactory.AddConsole() |> ignore
        if env.IsDevelopment() then
            app.UseDeveloperExceptionPage() |> ignore
        
        app.UseMvc() |> ignore
        app.UseWelcomePage() |> ignore